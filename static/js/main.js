$(document).ready(function(){
    var totalval = $('#id_orderitem_set-TOTAL_FORMS').val();
    // console.log(totalval);
    if(totalval == 1){
	var deleteBtn = $('form#add-order-form li').last();
	// console.log(deleteBtn);
	deleteBtn.css("display", "none");
    }
});

// Adding Name and Id 
function addNameId(domel, findel, total){
    domel.find(findel).each(function(){
	var name = $(this).attr('name').replace(
	    '-' + (total-1) + '-', '-' + total + '-'
	);
	var id = 'id_' + name;
	//console.log(id, name);
	$(this).attr({'name': name, 'id': id});
    });
}
// Adding Label
function addLabel(domel, total){
    domel.find('label').each(function (){
	var newFor = $(this).attr('for').replace(
	    '-' + (total-1) + '-', '-' + total + '-'
	);
	$(this).attr('for', newFor);
    });
}
// Adding Product page
function addproduct(){
    var newSelectElement = $('form#add-order-form li').eq(-3).clone(true);
    // var newInputElementA = $('form#add-order-form li').eq(-3).clone(true);
    var newInputElementQ = $('form#add-order-form li').eq(-2).clone(true);
    var newInputElementS = $('form#add-order-form li').last().clone(true);
    var total = $('#id_form-TOTAL_FORMS').val();

    // console.log(total);
    addNameId(newSelectElement, 'select', total);
    //addNameId(newInputElementA, 'input', total);
    addNameId(newInputElementQ, 'input', total);
    addNameId(newInputElementS, 'input', total);
    
    addLabel(newSelectElement, total);
    //addLabel(newInputElementA, total);
    addLabel(newInputElementQ, total);
    addLabel(newInputElementS, total);
    
    total++;
    $('#id_form-TOTAL_FORMS').val(total);
    $('form#add-order-form li').last().after(newSelectElement);
    //$('form#add-order-form li').last().after(newInputElementA);
    $('form#add-order-form li').last().after(newInputElementQ);
    $('form#add-order-form li').last().after(newInputElementS);
    // console.log(newSelectElement, newInputElement, total);
}

function updateproduct(){
    var newSelectElement = $("form#add-order-form li").eq(-4).clone(true);
    //var newInputElementA = $('form#add-order-form li').eq(-4).clone(true);
    var newInputElementQ = $('form#add-order-form li').eq(-3).clone(true);
    var newInputElementB = $('form#add-order-form li').eq(-2).clone(true);
    var newDeleteElement = $('form#add-order-form li').last().clone(true);
    var total = $('#id_orderitem_set-TOTAL_FORMS').val();
    
    addNameId(newSelectElement, 'select', total);
    addLabel(newSelectElement, total);
    //addNameId(newInputElementA, 'input', total);
    //addLabel(newInputElementA, total);
    addNameId(newInputElementQ, 'input', total);
    addLabel(newInputElementQ, total);
    addNameId(newInputElementB, 'input', total);
    addLabel(newInputElementB, total);
    addNameId(newDeleteElement, 'input', total);
    addLabel(newDeleteElement, total);

        
    total++;
    $('#id_orderitem_set-TOTAL_FORMS').val(total);
    //$('#id_orderitem_set-INITIAL_FORMS').val(total);
    $('form#add-order-form li').last().after(newSelectElement);
    //$('form#add-order-form li').last().after(newInputElementA);
    $('form#add-order-form li').last().after(newInputElementQ);
    $('form#add-order-form li').last().after(newInputElementB);
    $('form#add-order-form li').last().after(newDeleteElement);
}

function removeproduct(selector){
    var total = $('#id_form-TOTAL_FORMS').val();
    // console.log(total);
    if(total > 1){
	var removeSelectElement = $('form#add-order-form li').eq(-3).remove();
	//var removeInputElementA = $('form#add-order-form li').eq(-3).remove();
	var removeInputElementQ = $('form#add-order-form li').eq(-2).remove();
	var removeInputElementB = $('form#add-order-form li').last().remove();
	total--;
	$('#id_form-TOTAL_FORMS').val(total);
	// console.log(removeInputElement, removeSelectElement, total);
    }
}



$('#add-product').click(addproduct);
$('#remove-product').click(removeproduct);
$('#update-product').click(updateproduct);
