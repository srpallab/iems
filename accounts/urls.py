from django.urls import path
from . import views

urlpatterns = [
    path("login/", views.loginview, name="loginview"),
    path("logout/", views.logoutview, name="logoutview"),
]
