from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages


def loginview(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("dashboard")
        else:
            messages.info(request, "UserName or Password is incorrect")
    context = {}
    return render(request, "accounts/login.html", context)


def logoutview(request):
    logout(request)
    return redirect("loginview")
