from django.contrib import admin
from . import models


admin.site.register(models.Customer)
admin.site.register(models.Category)
admin.site.register(models.Expenses)
admin.site.register(models.EmployeeFund)


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    """
    Documentation for OrderAmin
    """

    list_display = ("id", "customer")


@admin.register(models.OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    """Documentation for OrderAmin

    """

    list_display = (
        "id", "order", "product", "quantities", "buying_sub_total", "sub_total"
    )


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    """Documentation for OrderAmin

    """

    list_display = ("id", "name", "code")


@admin.register(models.Employee)
class EmployeeAdmin(admin.ModelAdmin):
    """Documentation for OrderAmin

    """

    list_display = ("name", "phone")


@admin.register(models.Stock)
class StockAdmin(admin.ModelAdmin):
    """Documentation for OrderAmin

    """

    list_display = ("product", "buying_price", "stock_amount")


@admin.register(models.Roleback)
class RoleBackAdmin(admin.ModelAdmin):
    """
    Documentation for OrderAmin
    """

    list_display = ("order", "product", "stock", "quantities")
