from django.shortcuts import render
from .forms import (
    AddCategoryForm,
    AddProductForm,
    AddProductSetForm,
    AddStockForm,
    UpdateStockForm,
    AddCustomerForm,
    AddOrderForm,
    AddOrderCusForm,
    UpdateOrderForm,
    OrderItemFormSet,
    OrderItemFormSetUpdate,
    AddExpenses,
    UpdateExpenseForm,
)
from .models import (
    Category,
    Employee,
    Customer,
    Product,
    Stock,
    Order,
    OrderItem,
    Roleback,
    Expenses,
    EmployeeFund,
)
from django.db.models import Sum, Count
from datetime import datetime, timedelta
from decimal import Decimal
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from .filters import CustomerFilter, OrderFilter, InventoryFilter
from django.contrib.auth.decorators import login_required
from django.contrib import messages

# PDF import
from xhtml2pdf import pisa
from io import BytesIO
from django.template.loader import get_template


# from reportlab.pdfgen import canvas


def render_to_pdf(temp_src, context={}):
    """
    Render to PDF function takes a template and
    a context dictionary and covert it to a PDF.
    This function returns the PDF as a HTTP respose.
    """
    template = get_template(temp_src)
    html = template.render(context)
    # print(html.encode("utf-8"))
    result = BytesIO()
    # here we converting as a PDF
    pdf = pisa.pisaDocument(BytesIO(html.encode("utf-16")), result)
    # Caecking any PDF error
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type="application/pdf")
    # fileName = context["zone"] + ".pdf"
    # title = context["zone"]
    # customerName = context["qs"][0].customer.name
    # print(fileName, title, customerName)

    # def drawMyRuler(pdf):
    #    for i in range(100, 600, 100):
    #        stringWrite = "x" + str(i)
    #        # print(stringWrite)
    #        pdf.drawString(i, 810, stringWrite)
    #    for j in range(100, 900, 100):
    #        stringWrite = "y" + str(j)
    #        # print(stringWrite)
    #        pdf.drawString(10, j, stringWrite)

    # pdf = canvas.Canvas(fileName)
    # pdf.setTitle(title)
    # drawMyRuler(pdf)
    # pdf.drawString(100, 100, customerName)
    # pdf.save()
    # return HttpResponse(pdf, content_type="application/pdf")
    return print("Error")


@login_required(login_url="loginview")
def dashboard(request):
    """
    Dashboard displays only Product Stocks and Order Status.
    """
    # Showing All products stocks (Done)
    products_kg = Product.objects.filter(
        is_active=True).filter(is_amount=False)
    products_am = Product.objects.filter(is_active=True).filter(is_amount=True)
    products_stock = {}
    for product in products_kg:
        # Finding all Products last updated stocks
        t_stock = Stock.objects.filter(product=product.id).aggregate(
            stock=Sum("stock_amount")
        )
        products_stock[product.name] = [t_stock["stock"], product.id]
    for product in products_am:
        t_stock = (
            Stock.objects.filter(product=product.id).filter(
                stock_amount__gt=0).count()
        )
        products_stock[product.name] = [t_stock, product.id]

    # Showing Today Only Confirmed Products
    o_confirmed = Order.objects.filter(status="confirmed").filter(
        delivery_date=datetime.today()
    )
    products_confirmed = {}
    for confirme in o_confirmed:
        t_confirmed = confirme.orderitem_set.all()
        for pro_t in t_confirmed:
            if pro_t.product.name in products_confirmed:
                products_confirmed[pro_t.product.name] += pro_t.quantities
                print(products_confirmed)
            else:
                products_confirmed[pro_t.product.name] = pro_t.quantities
    print(products_confirmed)
    # Showing Only Today's Order by status (Done)
    today = datetime.now()
    statuses = {"confirmed": 0, "cancel": 0,
                "delivered": 0, "willcall": 0, "busy": 0}
    for status in statuses.keys():
        # counting all orders by status
        statuses[status] = (
            Order.objects.filter(status=status).filter(
                date_updated=today).count()
        )
    # passing products stock and  today's status
    context = {
        "products_stock": products_stock,
        "statuses": statuses,
        "products_confirmed": products_confirmed,
    }
    return render(request, "order/dashboard.html", context)


@login_required(login_url="loginview")
def product(request):
    """
    Product Display all product information in a table.
    We can add category and  product from here.
    """
    # Making it Django Class Based View (TODO)
    products = Product.objects.filter(is_active=True)
    categories = Category.objects.filter(is_active=True)
    stocks = Stock.objects.all().order_by("-id")[:5]
    context = {"products": products,
               "categories": categories, "stocks": stocks}
    return render(request, "order/product.html", context)


@login_required(login_url="loginview")
def addcategory(request):
    """
    Adding a Product Category with a form.
    Update Category Page (TODO)
    """
    # Making it Django Class Based View (TODO)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddCategoryForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            url = reverse("product")
            return HttpResponseRedirect(url)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddCategoryForm
        context = {"form": form}
    return render(request, "order/add-category.html", context)


@login_required(login_url="loginview")
def addproduct(request):
    """
    Adding a Product With a Form.
    """
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddProductForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            url = reverse("product")
            return HttpResponseRedirect(url)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddProductForm
        context = {"form": form}
    return render(request, "order/add-product.html", context)


@login_required(login_url="loginview")
def addbulkproduct(request, pk):
    """
    Adding a Product With a Form.
    """
    productObj = Product.objects.get(pk=pk)
    # print(productObj)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        formstock = AddProductSetForm(request.POST)
        # check whether it's valid:
        if formstock.is_valid():
            # process the data in form.cleaned_data as required
            for form in formstock:
                # print(form["stock_amount"].data, "data")
                if form["stock_amount"].data:
                    instance = form.save(commit=False)
                    instance.product = productObj
                    instance.save()
            # redirect to a new URL:
            url = reverse("product")
            return HttpResponseRedirect(url)

    # if a GET (or any other method) we'll create a blank form
    else:
        formstock = AddProductSetForm(queryset=Stock.objects.none())
        context = {"formstock": formstock}
    return render(request, "order/add-product.html", context)


@login_required(login_url="loginview")
def updateproduct(request, pk):
    productobj = Product.objects.get(pk=pk)
    # print(productobj)
    form = AddProductForm(instance=productobj)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddProductForm(request.POST, instance=productobj)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # redirect to a new URL:
            url = reverse("product")
            return HttpResponseRedirect(url)
    context = {"form": form}
    return render(request, "order/add-product.html", context)


@login_required(login_url="loginview")
def addproductstock(request, pk):
    productobj = Product.objects.get(pk=pk)
    # print(productobj)
    form = AddStockForm(initial={"product": productobj})
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddStockForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # redirect to a new URL:
            url = reverse("dashboard")
            return HttpResponseRedirect(url)
    context = {"form": form}
    return render(request, "order/update-stock.html", context)


@login_required(login_url="loginview")
def updateproductstock(request, pk):
    stockobj = Stock.objects.get(pk=pk)
    # print(productobj)
    form = UpdateStockForm(instance=stockobj)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = UpdateStockForm(request.POST, instance=stockobj)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # redirect to a new URL:
            url = reverse("product")
            return HttpResponseRedirect(url)
    context = {"form": form}
    return render(request, "order/update-stock.html", context)


@login_required(login_url="loginview")
def updatecategory(request, pk):
    categoryobj = Category.objects.get(pk=pk)
    # print(productobj)
    form = AddCategoryForm(instance=categoryobj)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddCategoryForm(request.POST, instance=categoryobj)
        # check whether it's valid:
        if form.is_valid():
            categoryobj.save()
            # redirect to a new URL:
            url = reverse("product")
            return HttpResponseRedirect(url)
    context = {"form": form}
    return render(request, "order/add-category.html", context)


@login_required(login_url="loginview")
def customer(request):
    customers_all = Customer.objects.all().order_by("-id")
    myFilter = CustomerFilter(request.GET, queryset=customers_all)
    customers_all = myFilter.qs
    perPage = request.GET.get("per-page")
    if perPage:
        perPage = int(perPage)
    else:
        perPage = 10
    paginator = Paginator(customers_all, perPage)
    page_number = request.GET.get("page")
    # print(paginator.count, paginator.num_pages,
    #      paginator.page_range, paginator.page(1))
    try:
        customers_all = paginator.page(page_number)
    except PageNotAnInteger:
        customers_all = paginator.page(1)
    except EmptyPage:
        customers_all = paginator.page(paginator.num_pages)
    context = {"customers": customers_all,
               "perPage": perPage, "myFilter": myFilter}
    return render(request, "order/customer.html", context)


@login_required(login_url="loginview")
def addcustomer(request):
    """
    Adding a Customer with a form.
    """
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddCustomerForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            url = reverse("customer")
            return HttpResponseRedirect(url)
        else:
            # messages.info()
            context = {"form": form}
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddCustomerForm()
        context = {"form": form}
    return render(request, "order/add-customer.html", context)


@login_required(login_url="loginview")
def updatecustomer(request, pk):
    customerObj = Customer.objects.get(pk=pk)
    # print(productobj)
    form = AddCustomerForm(instance=customerObj)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddCustomerForm(request.POST, instance=customerObj)
        # check whether it's valid:
        if form.is_valid():
            customerObj.save()
            # redirect to a new URL:
            url = reverse("customer")
            return HttpResponseRedirect(url)
        else:
            # messages.info()
            context = {"form": form}
    else:
        form = AddCustomerForm(instance=customerObj)
        context = {"form": form}
    return render(request, "order/add-customer.html", context)


@login_required(login_url="loginview")
def profile(request, pk):
    customer = Customer.objects.get(pk=pk)
    orders = Order.objects.filter(customer=customer)
    orders_count = orders.count()

    context = {"customer": customer, "orders": orders,
               "orders_count": orders_count}
    return render(request, "order/profile.html", context)


@login_required(login_url="loginview")
def orderview(request):
    orders = Order.objects.order_by("date_updated")
    myFilter = OrderFilter(request.GET, queryset=orders)
    orders = myFilter.qs
    # print(orders)
    context = {"orders": orders, "myFilter": myFilter}
    return render(request, "order/order.html", context)


@login_required(login_url="loginview")
def addorder(request):
    """
    Adding Order From Order Page.
    """
    tomorrow = datetime.now() + timedelta(1)
    if request.method == "GET":
        cusform = AddCustomerForm()
        form = AddOrderForm(initial={"delivery_date": tomorrow})
        formset = OrderItemFormSet(queryset=Product.objects.none())
    elif request.method == "POST":
        cus_name = request.POST.get("name")
        cus_address = request.POST.get("address")
        cur_phone = request.POST.get("phone")
        customer, created = Customer.objects.get_or_create(
            phone=cur_phone, defaults={
                "name": cus_name, "address": cus_address}
        )
        form = AddOrderForm(request.POST)
        formset = OrderItemFormSet(request.POST)
        if form.is_valid() and formset.is_valid():
            order = form.save(commit=False)
            order.customer = customer
            order.save()
            for form in formset:
                orderItem = form.save(commit=False)
                orderItem.order = order
                orderItem.sub_total = Decimal(
                    float(orderItem.selling_price) *
                    float(orderItem.quantities)
                )
                orderItem.save()
            # order.save()
            url = reverse("order")
            return HttpResponseRedirect(url)
    context = {"form": form, "formset": formset, "cusform": cusform}
    return render(request, "order/add-order.html", context)


@login_required(login_url="loginview")
def addordercus(request, pk):
    """
    Adding Order From Customer Page.
    """
    tomorrow = datetime.now() + timedelta(1)
    if request.method == "GET":
        customer = Customer.objects.get(pk=pk)
        form = AddOrderCusForm(
            initial={"customer": customer, "delivery_date": tomorrow}
        )
        formset = OrderItemFormSet(queryset=Product.objects.none())
    elif request.method == "POST":
        form = AddOrderCusForm(request.POST)
        formset = OrderItemFormSet(request.POST)
        if form.is_valid() and formset.is_valid():
            order = form.save()
            for form in formset:
                orderItem = form.save(commit=False)
                orderItem.order = order
                orderItem.sub_total = Decimal(
                    float(orderItem.selling_price) *
                    float(orderItem.quantities)
                )
                orderItem.save()
            # order.save()
            url = reverse("order")
            return HttpResponseRedirect(url)
    context = {"form": form, "formset": formset}
    return render(request, "order/add-order.html", context)


@login_required(login_url="loginview")
def updateorder(request, pk):
    orderObj = Order.objects.get(pk=pk)
    if request.method == "GET":
        form = UpdateOrderForm(instance=orderObj)
        formset = OrderItemFormSetUpdate(instance=orderObj)
    elif request.method == "POST":
        # print(request.POST)
        form = UpdateOrderForm(request.POST, instance=orderObj)
        formset = OrderItemFormSetUpdate(request.POST, instance=orderObj)
        # print(form)
        if form.is_valid() and formset.is_valid():
            # print(formset.deleted_forms)
            form.save()
            formset.save(commit=False)
            # print(formOrd)
            for deleteobjs in formset.deleted_objects:
                # print("delete: ", deleteobjs)
                deleteobjs.delete()
            for changedobjs in formset.changed_objects:
                # print("changed: ", changedobjs, type(changedobjs))
                # for formchn in changedobjs:
                #    print(type(formchn))
                changedobjs[0].sub_total = Decimal(
                    float(changedobjs[0].selling_price)
                    * float(changedobjs[0].quantities)
                )
                changedobjs[0].save()
                # oderItemObj = OrderItem.objects.get(pk=changedobjs)
            for newobjs in formset.new_objects:
                # print("new: ", newobjs, type(newobjs))
                newobjs.order = orderObj
                newobjs.sub_total = Decimal(
                    float(newobjs.selling_price) * float(newobjs.quantities)
                )
                newobjs.save()
            # formset.save()
            url = reverse("order")
            return HttpResponseRedirect(url)
    context = {"form": form, "formset": formset}
    return render(request, "order/update-order.html", context)


@login_required(login_url="loginview")
def deliverylist(request):
    zones = {
        "khilgaon": {"qs": None, "subtotal": [], "total": []},
        "uttara": {"qs": None, "subtotal": [], "total": []},
        "mohammadpur": {"qs": None, "subtotal": [], "total": []},
        "mirpur": {"qs": None, "subtotal": [], "total": []},
    }
    for zone in zones:
        print(request.GET.get("delivery_date"))
        if request.GET.get("delivery_date") == "GO":
            deliveryDate = request.GET.get("date")
            zones[zone]["qs"] = (
                Order.objects.filter(delivery_date=deliveryDate)
                .filter(status="confirmed")
                .filter(zone=zone)
            )

        else:
            zones[zone]["qs"] = (
                Order.objects.filter(delivery_date=datetime.now())
                .filter(status="confirmed")
                .filter(zone=zone)
            )
        # print(zones[zone]['qs'])
        for qs in zones[zone]["qs"]:
            orderitem = OrderItem.objects.filter(order=qs.id)
            # print(orderitem)
            subtotal = orderitem.aggregate(
                Sum("sub_total"), Count("sub_total"))
            total = float(subtotal["sub_total__sum"]) - \
                float(qs.discount) + 60.0
            # print(subtotal)

            zones[zone]["subtotal"].append(subtotal["sub_total__sum"])
            zones[zone]["total"].append(Decimal(total))
        # print(zones[zone])
    context = {"zones": zones}
    # Printing the delivery list
    if request.method == "GET":
        if request.GET.get("print") == "PRINT":
            if request.GET.get("zone") == "khilgaon":
                context = {
                    "zone": "khilgaon",
                    "qs": zones["khilgaon"]["qs"],
                    "subtotal": zones["khilgaon"]["subtotal"],
                    "total": zones["khilgaon"]["total"],
                }
                # print(context)
            elif request.GET.get("zone") == "uttara":
                context = {
                    "zone": "uttara",
                    "qs": zones["uttara"]["qs"],
                    "subtotal": zones["uttara"]["subtotal"],
                    "total": zones["uttara"]["total"],
                }
            elif request.GET.get("zone") == "mohammadpur":
                context = {
                    "zone": "mohammadpur",
                    "qs": zones["mohammadpur"]["qs"],
                    "subtotal": zones["mohammadpur"]["subtotal"],
                    "total": zones["mohammadpur"]["total"],
                }
            elif request.GET.get("zone") == "mirpur":
                context = {
                    "zone": "mirpur",
                    "qs": zones["mirpur"]["qs"],
                    "subtotal": zones["mirpur"]["subtotal"],
                    "total": zones["mirpur"]["total"],
                }
            pdf = render_to_pdf("order/pdf-order.html", context)
            return HttpResponse(pdf, content_type="application/pdf")
    return render(request, "order/delivery-list.html", context)


@login_required(login_url="loginview")
def updatestock(request, pk):
    """
    Update Stocks and Delivery Man fund
    """
    orderObj = Order.objects.get(pk=pk)
    orderObj.is_invoice = True
    if orderObj.employee:
        fundObj = EmployeeFund(employee=orderObj.employee,
                               fund_amount=Decimal(20.0))
        fundObj.order = orderObj
    else:
        messages.error(request, "Did you forget to add Delivery Man!")
        url = reverse("delivery-list")
        return HttpResponseRedirect(url)
    orderItemsqs = orderObj.orderitem_set.all()
    print(orderItemsqs)
    for item in orderItemsqs:
        stocks = (
            Stock.objects.filter(product=item.product)
            .filter(stock_amount__gt=0)
            .order_by("id")
        )
        item_quantity = item.quantities
        need_quantity = stocks.aggregate(Total=Sum("stock_amount"))
        if need_quantity["Total"] is None:
            need_quantity["Total"] = 0
            print(need_quantity["Total"])
        if float(need_quantity["Total"]) < float(item_quantity):
            messages.error(request, "Did you forget to Check Stock!")
            url = reverse("delivery-list")
            return HttpResponseRedirect(url)
        else:
            fundObj.save()
            orderObj.save()
        for stock in stocks:
            if item_quantity > 0:
                if item_quantity >= stock.stock_amount:
                    item_quantity -= stock.stock_amount
                    buying_price = item.buying_sub_total
                    buying_price += Decimal(
                        float(stock.buying_price) * float(stock.stock_amount)
                    )
                    item.buying_sub_total = buying_price
                    rolebackObj = Roleback.objects.create(
                        order=orderObj,
                        product=item.product,
                        stock=stock,
                        quantities=stock.stock_amount,
                    )
                    rolebackObj.save()
                    stock.stock_amount = 0
                    stock.save()
                    item.save()
                    print("Quantity: ", str(item_quantity), buying_price)
                elif item_quantity < stock.stock_amount:
                    buying_price = item.buying_sub_total
                    buying_price += Decimal(
                        float(stock.buying_price) * float(item_quantity)
                    )
                    item.buying_sub_total = buying_price
                    stock.stock_amount -= item_quantity
                    print("Quantity: ", str(item_quantity), buying_price)
                    rolebackObj = Roleback.objects.create(
                        order=orderObj,
                        product=item.product,
                        stock=stock,
                        quantities=item_quantity,
                    )
                    rolebackObj.save()
                    item_quantity = 0
                    item.save()
                    stock.save()
                    print("Quantity: ", str(item_quantity))
            else:
                break
    url = reverse("delivery-list")
    return HttpResponseRedirect(url)


@login_required(login_url="loginview")
def rolebackstock(request, pk):
    """
    Role Back Stocks If Done Mistake
    """
    orderObj = Order.objects.get(pk=pk)
    orderObj.is_invoice = False
    rolebacks = Roleback.objects.filter(order=orderObj)
    for roleback in rolebacks:
        stock = Stock.objects.get(id=roleback.stock.id)
        stock.stock_amount += roleback.quantities
        stock.save()
        item = orderObj.orderitem_set.get(product=stock.product.id)
        # print(item)
        item.buying_sub_total = 0
        item.save()
        roleback.delete()

    orderObj.save()
    # print(orderItemsqs)

    url = reverse("delivery-list")
    return HttpResponseRedirect(url)


@login_required(login_url="loginview")
def expenses(request):
    expensesObj = Expenses.objects.all()
    context = {"expenses": expensesObj}
    return render(request, "order/expenses.html", context)


@login_required(login_url="loginview")
def addexpenses(request):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AddExpenses(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            url = reverse("expenses")
            return HttpResponseRedirect(url)
        else:
            form = AddExpenses(request.POST)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = AddExpenses
    context = {"form": form}
    return render(request, "order/add-expenses.html", context)


@login_required(login_url="loginview")
def updateexpenses(request, pk):
    expenseObj = Expenses.objects.get(pk=pk)
    if request.method == "GET":
        form = UpdateExpenseForm(instance=expenseObj)
    elif request.method == "POST":
        form = UpdateExpenseForm(request.POST)
        if form.is_valid():
            form.save()
            url = reverse("expenses")
            return HttpResponseRedirect(url)
        else:
            form = UpdateExpenseForm(request.POST)
    context = {"form": form}
    return render(request, "order/add-expenses.html", context)


@login_required(login_url="loginview")
def inventory(request):
    today = datetime.now()
    if request.GET.get("submit") == "search":
        form_date = request.GET.get("start_date")
        to_date = request.GET.get("end_date")
        todays_selling = Order.objects.filter(is_invoice=True)
        if form_date == "" and to_date == "":
            today_expnce = Expenses.objects.filter(date_added=today).aggregate(
                Sum("sub_total")
            )
        else:
            today_expnce = (
                Expenses.objects.filter(date_added__gte=form_date)
                .filter(date_added__lte=to_date)
                .aggregate(Sum("sub_total"))
            )
    else:
        todays_selling = Order.objects.filter(is_invoice=True).filter(
            date_updated=today
        )
        today_expnce = Expenses.objects.filter(date_added=today).aggregate(
            Sum("sub_total")
        )
    # print(todays_selling)
    myFilter = InventoryFilter(request.GET, queryset=todays_selling)
    todays_selling = myFilter.qs
    total_selling = todays_selling.aggregate(
        total_selling=Sum("orderitem__sub_total"))
    # print(total_selling)
    total_buying = todays_selling.aggregate(
        total_buying=Sum("orderitem__buying_sub_total")
    )
    # print(total_buying)
    if today_expnce["sub_total__sum"]:
        total_profit = total_selling["total_selling"] - (
            total_buying["total_buying"] + today_expnce["sub_total__sum"]
        )
    elif total_buying["total_buying"]:
        total_profit = total_selling["total_selling"] - \
            (total_buying["total_buying"])
    else:
        total_profit = 0

    delivery_man = Employee.objects.all()
    funds = {}
    for man in delivery_man:
        funds[man.name] = (
            EmployeeFund.objects.filter(employee=man)
            .filter(date_added=today)
            .aggregate(Sum("fund_amount")),
            EmployeeFund.objects.filter(employee=man).filter(
                date_added=today).count()
            * 40,
        )
    # print(funds)
    context = {
        "total_selling": total_selling["total_selling"],
        "total_buying": total_buying["total_buying"],
        "today_expnces": today_expnce["sub_total__sum"],
        "total_profit": total_profit,
        "funds": funds,
        "myFilter": myFilter,
    }
    return render(request, "order/inventory.html", context)
