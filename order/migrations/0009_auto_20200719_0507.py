# Generated by Django 3.0.8 on 2020-07-18 23:07

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_auto_20200719_0420'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='delivery_date',
            field=models.DateField(default=datetime.datetime(2020, 7, 20, 5, 7, 58, 435883)),
        ),
        migrations.AlterField(
            model_name='order',
            name='zone',
            field=models.CharField(choices=[('khilgaon', 'Khilgaon'), ('uttara', 'Uttara'), ('gulshan', 'Gulshan'), ('mirpur', 'Mirpur')], max_length=35, null=True),
        ),
    ]
