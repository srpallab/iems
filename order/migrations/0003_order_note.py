# Generated by Django 3.0.8 on 2020-07-12 14:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_auto_20200712_0144'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='note',
            field=models.CharField(default='none', max_length=255),
        ),
    ]
