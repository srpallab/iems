# Generated by Django 3.0.8 on 2020-08-29 11:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0018_auto_20200819_2231'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='buying_price',
        ),
        migrations.RemoveField(
            model_name='product',
            name='selling_price',
        ),
        migrations.RemoveField(
            model_name='product',
            name='stock',
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('buying_price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('selling_price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('stock', models.DecimalField(decimal_places=2, max_digits=19)),
                ('date_added', models.DateField(auto_now_add=True)),
                ('date_updated', models.DateField(auto_now=True)),
                ('product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='order.Product')),
            ],
        ),
    ]
