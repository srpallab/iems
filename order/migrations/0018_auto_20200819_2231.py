# Generated by Django 3.0.8 on 2020-08-19 16:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0017_auto_20200804_0635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='note',
            field=models.CharField(default='N/A', max_length=255),
        ),
    ]
