from django.forms import (
    ModelForm,
    Textarea,
    NumberInput,
    DateInput,
    modelformset_factory,
    inlineformset_factory,
)
from .models import (Product, Stock, Category, Customer,
                     Order, OrderItem, Expenses)


class DateInput(DateInput):
    input_type = "date"


class AddCategoryForm(ModelForm):
    """
    Form for adding category
    """

    class Meta:
        model = Category
        exclude = ["date_added", "date_updated"]


class AddProductForm(ModelForm):
    """
    Form for adding products
    """

    class Meta:
        model = Product
        exclude = ["date_added", "date_updated"]


AddProductSetForm = modelformset_factory(
    Stock,
    fields=("buying_price", "stock_amount"),
    extra=5
)


class AddStockForm(ModelForm):
    """
    Form for adding Stocks record
    """

    class Meta:
        model = Stock
        exclude = ["date_added", "date_updated"]


class UpdateStockForm(ModelForm):
    """
    Form for adding Stocks record from update product page
    """

    class Meta:
        model = Stock
        exclude = ["date_added", "date_updated"]


class AddCustomerForm(ModelForm):
    """
    Form for adding products
    """

    class Meta:
        model = Customer
        exclude = ["is_member", "date_added", "date_updated"]
        widgets = {"address": Textarea(attrs={})}
        # "cols": 80, "rows": 20


class AddOrderForm(ModelForm):
    """
    Form for adding orders
    """

    class Meta:
        model = Order
        fields = ["status", "zone", "note", "delivery_date"]
        widgets = {
            "delivery_date": DateInput(),
        }


class AddOrderCusForm(ModelForm):
    """
    Form for adding orders
    """

    class Meta:
        model = Order
        fields = ["customer", "status", "zone", "note", "delivery_date"]
        widgets = {
            "delivery_date": DateInput(),
        }


class UpdateOrderForm(ModelForm):
    """
    Form for adding orders
    """

    class Meta:
        model = Order
        fields = [
            "customer",
            "status",
            "zone",
            "employee",
            "delivery_date",
            "delivery_charge",
            "discount",
            "note",
            "payment_method",
        ]
        widgets = {
            "delivery_date": DateInput(),
        }


OrderItemFormSet = modelformset_factory(
    OrderItem,
    fields=("product", "quantities", "selling_price"),
    extra=1,
    widgets={"quantities": NumberInput(attrs={"required": "required"})},
)

OrderItemFormSetUpdate = inlineformset_factory(
    Order,
    OrderItem,
    fields=("product", "quantities", "selling_price"),
    extra=0,
    widgets={"quantities": NumberInput(attrs={"required": "required"})},
)


class AddExpenses(ModelForm):
    """
    Expenses adding Form
    """

    class Meta:
        model = Expenses
        exclude = ["date_added", "date_updated"]


class UpdateExpenseForm(ModelForm):
    """
    Documentation for UpdateExpenseForm
    """

    class Meta:
        model = Expenses
        exclude = ["date_added", "date_updated"]
