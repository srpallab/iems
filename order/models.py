from django.db import models


class Employee(models.Model):
    """
    Employee Records Table
    """

    name = models.CharField(max_length=60)
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=11)
    is_delivery_man = models.BooleanField(default=True)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    """
    Customer Records Table
    """

    name = models.CharField(max_length=60)
    address = models.CharField(max_length=255)
    phone = models.CharField(max_length=11, unique=True)
    is_member = models.BooleanField(default=False)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    Category Records Table
    """

    name = models.CharField(max_length=60)
    is_active = models.BooleanField(default=True)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    """
    Product Records Table
    """

    name = models.CharField(max_length=60)
    code = models.CharField(max_length=25, unique=True)
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True)
    is_active = models.BooleanField(default=True)
    is_amount = models.BooleanField(default=True)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class Stock(models.Model):
    """
    Products buying_price, selling_price and stocks update
    """

    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    buying_price = models.DecimalField(max_digits=19, decimal_places=2)
    stock_amount = models.DecimalField(max_digits=19, decimal_places=2)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.id)


class Order(models.Model):
    """
    Order Records Table
    """

    STATUS_CHOICES = (
        ("confirmed", "Confirmed"),
        ("cancel", "Cancel"),
        ("willcall", "Will Call"),
        ("busy", "Busy"),
    )
    ZONE = (
        ("khilgaon", "Khilgaon"),
        ("uttara", "Uttara"),
        ("mohammadpur", "Mohammadpur"),
        ("mirpur", "Mirpur"),
    )
    customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, null=True)
    employee = models.ForeignKey(
        Employee, on_delete=models.SET_NULL, null=True)
    delivery_date = models.DateField()
    status = models.CharField(max_length=35, choices=STATUS_CHOICES)
    zone = models.CharField(max_length=35, choices=ZONE, null=True)
    payment_method = models.CharField(default="cash", max_length=100)
    is_invoice = models.BooleanField(default=False)
    delivery_charge = models.DecimalField(
        default=60.00, max_digits=9, decimal_places=2)
    discount = models.DecimalField(default=0.0, max_digits=5, decimal_places=2)
    note = models.CharField(max_length=255, default="N/A")
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.id)


class OrderItem(models.Model):
    """
    OrderItems Records Table
    """
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    selling_price = models.DecimalField(max_digits=19, decimal_places=2)
    quantities = models.DecimalField(max_digits=19, decimal_places=3)
    buying_sub_total = models.DecimalField(
        max_digits=19, decimal_places=3, default=0)
    sub_total = models.DecimalField(max_digits=19, decimal_places=3)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.id)


class Roleback(models.Model):
    """
    OrderItems Records Table
    """
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    stock = models.ForeignKey(Stock, on_delete=models.SET_NULL, null=True)
    quantities = models.DecimalField(max_digits=19, decimal_places=3)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.id)


class EmployeeFund(models.Model):
    """
    Purchase Records Table
    """

    employee = models.ForeignKey(
        Employee, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(
        Order, on_delete=models.SET_NULL, null=True)
    fund_amount = models.DecimalField(
        default=0.0, max_digits=19, decimal_places=2)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.id)


class Expenses(models.Model):
    """
    Expances Records Table
    """

    EXPENCES = (("office", "Office"), ("delivery",
                                       "Delivery"), ("salary", "Salary"))

    expances_head = models.CharField(max_length=255)
    sub_total = models.DecimalField(
        default=0.0, max_digits=19, decimal_places=2)
    expances_type = models.CharField(max_length=50, choices=EXPENCES)
    date_added = models.DateField(auto_now_add=True)
    date_updated = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.id)
