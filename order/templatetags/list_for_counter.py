from django import template

register = template.Library()


@register.filter(name="nextindex")
def nextindex(arr, counter):
    return arr[counter]
