from django_filters import FilterSet, CharFilter, DateFilter
from django.forms import DateInput
from .models import Customer, Order


class CustomerFilter(FilterSet):
    """
    Filter Class for Customers
    """

    name = CharFilter(label="Name", field_name="name", lookup_expr="icontains")
    address = CharFilter(
        label="Address", field_name="address", lookup_expr="icontains")
    phone = CharFilter(label="Phone", field_name="phone",
                       lookup_expr="icontains")

    class Meta:
        model = Customer
        fields = ["name", "phone", "address"]


class DateInput(DateInput):
    input_type = "date"


class OrderFilter(FilterSet):
    """
    Filter Class for Order
    """

    date_added = DateFilter(label="Date", widget=DateInput())

    class Meta:
        model = Order
        fields = ["status", "date_added"]


class InventoryFilter(FilterSet):
    """
    Documentation for Inventory
    """

    start_date = DateFilter(
        label="Start Date",
        field_name="date_added",
        widget=DateInput(),
        lookup_expr="gte",
    )
    end_date = DateFilter(
        label="End Date",
        field_name="date_added",
        widget=DateInput(),
        lookup_expr="lte",
    )

    class Meta:
        model = Order
        fields = []
